﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadSlide : MonoBehaviour {

    public Logic_PechaKucha pechaLogic;
    public int slideToLoad;

    public void Start() {
    Button but =    GetComponent<Button>() as Button;
    if (but)
        but.onClick.AddListener(LoadSlideSelected);
            
    }


    public void LoadSlideSelected() 
    {
        if (pechaLogic)
            pechaLogic.LoadSlide(slideToLoad);
    }
}
