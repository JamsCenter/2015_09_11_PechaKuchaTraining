﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PechaKuchaDataMono : MonoBehaviour
{

    public PechaKuchaData data;

  
   
}

[System.Serializable]
public class PechaKuchaData {


    public string m_title;
    public TwitchPitch m_twitchPitch;
    public FourWordsPitch m_fourWord;
    public Slide [] m_slide = new Slide[20];
    public Texture2D m_backgroundTexture;

    internal Texture2D GetTexture(int slideIndex)
    {
        slideIndex= Mathf.Clamp(slideIndex,0, 19 );
       return  m_slide[slideIndex].m_texture;
    }

    internal FourWordsPitch GetFourWords(int slideIndex)
    {
        slideIndex = Mathf.Clamp(slideIndex, 0, 19 );
        return m_slide[slideIndex].m_fourWords;
    }
}


[System.Serializable]
public class TwitchPitch {
    public int m_twitchLengh=140;
    public string m_pitch;
}

[System.Serializable]
public class FourWordsPitch {

    public string word1;
    public string word2;
    public string word3;
    public string word4;
}

[System.Serializable]
public class Slide {

    public Texture2D m_texture;
    public FourWordsPitch m_fourWords;
}
