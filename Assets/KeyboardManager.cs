﻿using UnityEngine;
using System.Collections;

public class KeyboardManager : MonoBehaviour {

    public Logic_PechaKucha pechaLogic;

	void Update () {

        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.Clear))
            pechaLogic.SetPreviousSlide();
        if (Input.GetKeyDown(KeyCode.RightArrow))
            pechaLogic.SetNextSlide();

        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
            pechaLogic.SwitchPause();
        if (Input.GetKeyDown(KeyCode.R) || Input.GetKeyDown(KeyCode.Delete))
            pechaLogic.Restart();

	
	}
}
