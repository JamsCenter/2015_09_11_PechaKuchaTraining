﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour {

    public string sceneNameToLoad ="";
    public float withDelay=2f;

    public UnityEvent m_onStartLoading;

    public UnityEvent m_beforeLoading;

    public delegate void DoBeforeLoadNextScene(string currentScene, string nextScene);
    public static DoBeforeLoadNextScene WhatToDoBeforeLoadNextScene;
    public delegate void DoAtLoadWithDelayNextScene(string currentScene, string nextScene,float delay);
    public static DoAtLoadWithDelayNextScene OnStartLoadWithDelay;
    public void LoadSelectedSceneWithDelay()
    {
        if (withDelay > 0){
            m_onStartLoading.Invoke();
            if (OnStartLoadWithDelay != null)
                OnStartLoadWithDelay(SceneManager.GetActiveScene().name, sceneNameToLoad, withDelay);
            Invoke("LoadSelectedScene", withDelay);
        }
        else LoadSelectedScene();
    }
    
    public void LoadSelectedScene()
    {
        LoadNextScene(sceneNameToLoad);
    }
    public void LoadCurrentScene()
    {
        LoadNextScene(SceneManager.GetActiveScene().name);
    }
    public void LoadSelectedScene(string sceneName)
    {
        LoadNextScene(sceneName);
    }
    public  void LoadNextScene(string nextScene) {
        try {

            m_beforeLoading.Invoke();
            if (WhatToDoBeforeLoadNextScene != null)
                WhatToDoBeforeLoadNextScene(SceneManager.GetActiveScene().name, nextScene);
            if (string.IsNullOrEmpty(nextScene))
                Application.Quit();
            else
                SceneManager.LoadScene(nextScene);

        }
        catch(Exception e)
        {
            Debug.LogWarning("Impossible to load next scene:" + e);
        }
    }
}
