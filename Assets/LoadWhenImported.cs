﻿using UnityEngine;
using System.Collections;

public class LoadWhenImported : MonoBehaviour {

    public ImportPechaKuchaFromFolder importFromFolder;


    public LoadScene loader;

	void Update () {

        if (importFromFolder.finishImportation)
            loader.LoadSelectedScene();
	}
}
