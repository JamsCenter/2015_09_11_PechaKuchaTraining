﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Text.RegularExpressions;

public class ImportPechaKuchaFromFolder : MonoBehaviour {


    public bool finishImportation;
    public string androidStorageFolder = "/sdcard/PechaKucha/";

    //public string windowStorageFolder = "/sdcard/PechaKucha/";
    //public string iosStorageFolder = "/sdcard/PechaKucha/";
    public string windowsNextToUnityExe = "PechaKucha";
    public string[] filesExtension = new string[] { ".xml",".jpeg", ".jpg", ".png" };
    public PechaKucha lastFound;
    void Start() {
        ImportRooms();
        finishImportation = true;
        //string text = "Eloi est aller à ldkqjfmdj flkjqdskf";
        //string integerOut = Regex.Match(text, @"\d+").Value;
        //Debug.Log(integerOut);
    }

    public List<PechaKucha> ImportRooms()
    {
        List<PechaKucha> pechaFound = new List<PechaKucha>();
        try
        {
            string path = windowsNextToUnityExe;
#if UNITY_EDITOR
            path = Application.dataPath + "/Resources/" + windowsNextToUnityExe + "/";
#elif UNITY_ANDROID 
                path = androidStorageFolder;
#endif

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                Debug.Log("Path did not existe, created at:" + path);
            }

            string [] pechaFoldersPath = Directory.GetDirectories(path);
            
            for (int i = 0; i < pechaFoldersPath.Length; i++)
			{
                string folderPath = pechaFoldersPath[i];
                string folderName = GetFolderName(folderPath);

                Texture2D[] imagesFound = FoundImage(folderPath);

                PechaKucha pecha = new PechaKucha(folderName,imagesFound);
                Debug.Log("Folder Name: " + pecha._title);

                lastFound = pecha;

			}

        }
        catch (Exception e)
        {
            Debug.LogException(e, this.gameObject);
        }

        return pechaFound;
    }

    private string GetFolderName(string folderPath)
    {
        string folderName = "";
        int backSlashIndex = -1;
        backSlashIndex = folderPath.LastIndexOf('/');
        if (backSlashIndex == -1)
            folderName = folderPath;
        else folderName = folderPath.Substring(backSlashIndex + 1);
        return folderName;
    }

    private Texture2D[] FoundImage(string folderPath)
    {
        //List<Sprite> imagesFound=new List<Sprite>();
        Texture2D[] imagesFoundWithNumber = new Texture2D[20];
        string[] pechaFilesInFolder = Directory.GetFiles(folderPath);

        for (int i = 0; i < pechaFilesInFolder.Length; i++)
        {
            string imagePath = pechaFilesInFolder[i];
            if (IsFileIsImage(imagePath))
            {
                Texture2D imageTexture = LoadPNG(imagePath);
                imageTexture.Compress(false);
                 //imagesFound.Add(sprite);

                ///
                
                string imageName = GetFolderName(imagePath);
                string integerOut = Regex.Match(imageName, @"\d+").Value;
                int indexFound =-1;
                if (int.TryParse(integerOut,out indexFound)) 
                {
                    if (indexFound >= 0 && indexFound <= 20)
                    {
//                        Sprite sprite = Sprite.Create(imageTexture, new Rect(0, 0, imageTexture.width, imageTexture.height), Vector2.one / 2f);
                        imagesFoundWithNumber[indexFound] = imageTexture;
                    }
 
                }
            }
            
        }


        return imagesFoundWithNumber;
    }

    private bool IsFileIsImage(string imagePath)
    {
        for (int i = 0; i < filesExtension.Length; i++)
        {
            if (imagePath.IndexOf( filesExtension[i])!=-1 && imagePath.IndexOf(".meta")==-1)
                return true;
        }
        return false;
    }

    public static Texture2D LoadPNG(string filePath)
    {

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }
}
