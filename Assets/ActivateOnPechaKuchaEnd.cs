﻿using UnityEngine;
using System.Collections;

public class ActivateOnPechaKuchaEnd : MonoBehaviour {


    public Logic_PechaKucha pechaLogic;
    public GameObject[] toActivate;

    void Start()
    {
        pechaLogic.onPresentationEnd += ActivateOnEnd;
        pechaLogic.onPresentationStart += DeactivateOnEnd;
	}

    private void ActivateOnEnd()
    {
        SetActivateOnEnd(true);
    }
    private void DeactivateOnEnd()
    {
        SetActivateOnEnd(false);
    }
    private void SetActivateOnEnd(bool onOff=true)
    {
        for (int i = 0; i < toActivate.Length; i++)
        {
            toActivate[i].SetActive(onOff);
        }
    }
	
	
}
