﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Logic_PechaSelector : MonoBehaviour {

   public  UI_PechaSelectorSimple uiPechaSelector;
   public PechaKucha[] pechaInstanciated;
   public int selectionIndex;
   void Start() {
       pechaInstanciated = PechaKucha.GetAll();
       if (pechaInstanciated.Length == 0)
       {
           uiPechaSelector.SetAsNoneLoaded();
           return;
       }
       SetToIndex(0);
       
   }
   public void SetToIndex(int index)
   {
       Mathf.Clamp(index, 0, pechaInstanciated.Length);
       PechaKucha select = pechaInstanciated[index];
       selectionIndex = index;
       PechaKucha.Selected = select;
       uiPechaSelector.SetUITo(select._title, select.GetFirstImage(), index - 1 >= 0, index +1 <pechaInstanciated.Length);


   }
   public void SelectNextPecha() { SetToIndex(selectionIndex++); }
   public void SelectPreviousPecha() { SetToIndex(selectionIndex--); }


}
