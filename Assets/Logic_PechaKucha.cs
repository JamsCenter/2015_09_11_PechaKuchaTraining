﻿using UnityEngine;
using System.Collections;
using System;

public class Logic_PechaKucha : MonoBehaviour {

    public UI_PechaKucha uiPecha;
    public bool playCountDown;
    public float timeLeft=20;
    public int _slide;

    public delegate void OnPresentationChangeDetected();
    public OnPresentationChangeDetected onPresentationEnd;

    

    public OnPresentationChangeDetected onPresentationStart;
    private bool endDetected;


    public PechaKuchaDataMono m_default;
    public PechaKuchaDataMono m_accessPeckaKucha;

    public int Slide
    {
        get { return _slide; }
        set
        {
            _slide = Mathf.Clamp(value, 0, 20); ;
        }
    }
    
	void Start () {
        Restart();
	}
	
	void Update () {
        if (!playCountDown) return;
        if (Slide >= 20) {
            timeLeft = 0f;
            SetSlideTo(19);
        }
        if (timeLeft > 0f && Slide<20)
        {
            timeLeft -= Time.deltaTime;

            if (timeLeft < 0f) 
                timeLeft = 0f;
            if (timeLeft == 0f) {
                Slide ++;
                SetSlideTo(Slide);
                timeLeft = Slide < 20 ? 20f : 0f;

            }

        }
        uiPecha.SetTimer(timeLeft);
        if (!endDetected && timeLeft <= 0f && Slide >= 19)
        {
            endDetected = true;
            if (onPresentationEnd != null)
                onPresentationEnd();
        }
        

	}

  



    public void SetTimeLeft(float pourcent) {
        timeLeft = 20f * pourcent;
    }


    public void SetPreviousSlide() {
        Slide--;
        SetSlideTo(Slide);
        ResetTimer();
    }
    public void SetNextSlide() 
    {
        Slide++;
        SetSlideTo(Slide);
        ResetTimer();
    }
    public void ResetTimer() { 
        timeLeft = 20; uiPecha.SetTimer(20f);
    }
    public void Restart() {


        if (onPresentationStart != null)
            onPresentationStart();
        endDetected = false;

        ResetTimer();
        Slide = 0;
        SetSlideTo(Slide);


    }

    public void SetSlideTo(int slideIndex)
    {
        ResetTimer();
        Slide = slideIndex;
        uiPecha.SetSlideLeft(20 - slideIndex);
        Texture2D texture = m_accessPeckaKucha.data.GetTexture(slideIndex);
        if(texture==null) texture = m_default.data.GetTexture(slideIndex);

        FourWordsPitch fourWords = m_accessPeckaKucha.data.GetFourWords(slideIndex);
        if (fourWords == null) texture = m_default.data.GetTexture(slideIndex);

        uiPecha.SetImage(texture);
        uiPecha.SetFourWords(fourWords.word1, fourWords.word2, fourWords.word3, fourWords.word4);


    }
    public void Play() { playCountDown = true; }
    public void Stop() { playCountDown = false; }
    public void SwitchPause() { playCountDown = !playCountDown; }

    public void LoadSlide(int slideToLoad)
    {
        SetSlideTo(slideToLoad);
        ResetTimer();
    }

  

    public void SaveFourWords() {

        string w1, w2, w3, w4;
        uiPecha.GetFourCurrentWords(out w1, out w2, out w3, out w4);
        m_accessPeckaKucha.data.m_slide[_slide].m_fourWords.word1 = w1;
        m_accessPeckaKucha.data.m_slide[_slide].m_fourWords.word2 = w2;
        m_accessPeckaKucha.data.m_slide[_slide].m_fourWords.word3 = w3;
        m_accessPeckaKucha.data.m_slide[_slide].m_fourWords.word4 = w4;


    }
    
}
