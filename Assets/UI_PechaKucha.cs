﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class UI_PechaKucha : MonoBehaviour {


    public RawImage imageDisplay;
    public AspectRatioFitter imageAspect;
    public Text tTimer;
    public Text tSlider;

    public Image iPourcentTimeLeft;
    public Image[] iSlideLeft;


    public Text title;
    public RawImage background;


    public GameObject escapeMenu;
    public InputField m_word_1;
    public InputField m_word_2;
    public InputField m_word_3;
    public InputField m_word_4;

    public void SetTimer(float timeLeft) {
        timeLeft = Mathf.Clamp(timeLeft, 0f, 20f);
        iPourcentTimeLeft.fillAmount = timeLeft / 20f;
        tTimer.text = "" + (int)(timeLeft+0.999f);
    }
    public void SetSlideLeft(int slideLeft) {

        slideLeft = Mathf.Clamp(slideLeft, 0, 20);
        tSlider.text = "" + slideLeft;
        for (int i = 0; i < 20; i++)
        {
            if(i<iSlideLeft.Length){
                Color color = Color.white;
                color.a = i <= slideLeft-1?1f:0f;
                iSlideLeft[i].color = color;
            
            }
        }
    }





    internal void SetImage(Texture2D texture)
    {
        imageDisplay.texture = texture;
        imageAspect.aspectRatio = texture.width / (float) texture.height;
    }

    internal void SetTitle(string title)
    {
        this.title.text = title;
    }

    internal void SetBackground(Texture2D background)
    {
        if(background!=null)
        this.background.texture = background;
    }
    public void SetFourWords( string word1, string word2, string word3, string word4)
    {
        if (word1 == "") { word1 = " "; }
        if (word2 == "") { word2 = " "; }
        if (word3 == "") { word3 = " "; }
        if (word4 == "") { word4 = " "; }
        m_word_1.text = word1;
        m_word_2.text = word2;
        m_word_3.text = word3;
        m_word_4.text = word4;
    }

    internal void GetFourCurrentWords(out string word1, out string word2, out string word3, out string word4)
    {
        word1 = m_word_1.text;
         word2= m_word_2.text;
         word3= m_word_3.text;
         word4= m_word_4.text;
    }
}
